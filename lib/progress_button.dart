import 'package:flutter/material.dart';

class ProgressButtonController extends ChangeNotifier {
  bool _showProgress = false;

  void showProgress(bool progress) {
    _showProgress = progress;
  }
}

class ProgressButton extends StatefulWidget {
  final ProgressButtonController controller;
  final void Function()? onPressed;
  final Widget child;
  final double? width;

  const ProgressButton(
      {Key? key,
      this.width,
      required this.child,
      required this.controller,
      required this.onPressed})
      : super(key: key);

  @override
  State<ProgressButton> createState() => _ProgressButtonState();
}

class _ProgressButtonState extends State<ProgressButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: ElevatedButton(
        onPressed: widget.controller._showProgress ? null : widget.onPressed,
        child: widget.controller._showProgress == false
            ? widget.child
            : const SizedBox(
                width: 15,
                height: 15,
                child: CircularProgressIndicator(strokeWidth: 3)),
      ),
    );
  }
}
