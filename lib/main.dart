import 'package:flutter/material.dart';
import 'package:progressbutton/progress_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Progress Button Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final buttonController = ProgressButtonController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Progess button example')),
      body: Center(
          child: ProgressButton(
              width: 100,
              child: const Text('Press Me'),
              controller: buttonController,
              onPressed: () async {
                setState(() {
                  buttonController.showProgress(true);
                });
                // Some long running task
                await Future.delayed(const Duration(seconds: 2));
                setState(() {
                  buttonController.showProgress(false);
                });
              })),
    );
  }
}
