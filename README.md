# progressbutton

A button which shows a circular progress indicator once pressed.
A controller is used to communicate the state to the button.

![](doc/demo.gif) 
